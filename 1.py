import requests # импортируем модуль
import subprocess
import py7zr
import psycopg2
import csv

# # Скачивание файлов
# newFile = open(r'zno2020.7z',"wb") # открываем файл для записи, в режиме wb
# fileGET = requests.get("https://zno.testportal.com.ua/yearstat/uploads/OpenDataZNO2020.7z") # делаем запрос
# newFile.write(fileGET.content) # записываем содержимое в файл; как видите - content запроса
# newFile.close()
# print("Архив OpenDataZNO2020.7z скачан")

# newFile = open(r'zno2019.7z',"wb") # открываем файл для записи, в режиме wb
# fileGET = requests.get("https://zno.testportal.com.ua/yearstat/uploads/OpenDataZNO2019.7z") # делаем запрос
# newFile.write(fileGET.content) # записываем содержимое в файл; как видите - content запроса
# newFile.close()
# print("Архив OpenDataZNO2019.7z скачан")

# # Разархивация
# archive = py7zr.SevenZipFile('zno2020.7z', mode='r')
# archive.extractall(path="unzipped")
# archive.close()
# print("База данных Odata2020File.csv разархивирована")

# archive = py7zr.SevenZipFile('zno2019.7z', mode='r')
# archive.extractall(path="unzipped")
# archive.close()
# print("База данных Odata2019File.csv разархивирована")

# with open("result2020.csv", 'w', newline = '') as csvfile:
#     writer = csv.writer(csvfile, delimiter=";")

#     with open("unzipped\\Odata2020File.csv", newline = '') as csvfile:
#     	reader = csv.DictReader(csvfile,delimiter=";")
    
#     	writer.writerow(["OUTID","REGNAME","UkrTest","UkrTestStatus","UkrBall100"])
#     	for row in reader:
#     		writer.writerow([row['OUTID'],row['REGNAME'],row['UkrTest'],row['UkrTestStatus'],row['UkrBall100']])
# print("Создана таблица csv result2020.csv")

# with open("result2019.csv", 'w', newline = '') as csvfile:
#     writer = csv.writer(csvfile, delimiter=";")

#     with open("unzipped\\Odata2019File.csv", newline = '') as csvfile:
#     	reader = csv.DictReader(csvfile,delimiter=";")
    
#     	writer.writerow(["OUTID","REGNAME","UkrTest","UkrTestStatus","UkrBall100"])
#     	for row in reader:
#     		writer.writerow([row['OUTID'],row['REGNAME'],row['UkrTest'],row['UkrTestStatus'],row['UkrBall100']])
# print("Создана таблица csv result2019.csv")

# Подключение к базе данных
con = psycopg2.connect(
  database="vlad", 
  user="postgres", 
  password="postgres", 
  host="127.0.0.1", 
  port="5432"
)
print("Database opened successfully")

cur = con.cursor() 

# Создание таблицы zno
cur.execute('''CREATE TABLE IF NOT EXISTS zno  
     (ID SERIAL PRIMARY KEY NOT NULL,
     OUTID TEXT UNIQUE,
     REGNAME TEXT,
     UkrTest TEXT,
     UkrTestStatus TEXT,
     UkrBall100 REAL,
     Year INTEGER
     );''')
print("Создана таблица zno")

# # 2020
# k = 0
# with open("result2020.csv") as zno2020_file: # Открыть файл result2020.csv как zno2020_file
#     file_reader = csv.reader(zno2020_file, delimiter=';')

#     try:
#         for row in file_reader: # row - строка (список, который содержит содержимое строки в csv файле)
#                 if k > 0:
#                 	OUTID = row[0]
#                 	REGNAME = row[1]
#                 	UkrTest = row[2]
#                 	UkrTestStatus = row[3]
#                 	if row[4] == 'null':
#                 		UkrBall100 = 0
#                 	else:
#                 		UkrBall100 = float(row[4].replace(',', '.'))
#                 	print(k)
#                 	cur.execute('''
#                     	INSERT INTO zno(OUTID,REGNAME,UkrTest,UkrTestStatus,UkrBall100,Year) 
#                     	VALUES (%s,%s,%s,%s,%s,%s) ON CONFLICT (OUTID) DO NOTHING;''', 
#                     	(OUTID, REGNAME, UkrTest, UkrTestStatus, UkrBall100, 2020)
#                     	)
#                 k += 1
#     except:
#         print('Error on the line ')
#         raise
#     finally:
#         zno2020_file.close()
#         con.commit()

# # 2019
# k = 0
# with open("result2019.csv") as zno2019_file: # Открыть файл result2019.csv как zno2019_file
#     file_reader = csv.reader(zno2019_file, delimiter=';')

#     try:
#         for row in file_reader: # row - строка (список, который содержит содержимое строки в csv файле)
#                 if k > 0:
#                 	OUTID = row[0]
#                 	REGNAME = row[1]
#                 	UkrTest = row[2]
#                 	UkrTestStatus = row[3]
#                 	if row[4] == 'null':
#                 		UkrBall100 = 0
#                 	else:
#                 		UkrBall100 = float(row[4].replace(',', '.'))
#                 	print(k)
#                 	cur.execute('''
#                     	INSERT INTO zno(OUTID,REGNAME,UkrTest,UkrTestStatus,UkrBall100,Year) 
#                     	VALUES (%s,%s,%s,%s,%s,%s) ON CONFLICT (OUTID) DO NOTHING;''', 
#                     	(OUTID, REGNAME, UkrTest, UkrTestStatus, UkrBall100, 2019)
#                     	)
#                 k += 1
#     except:
#         print('Error on the line ')
#         raise
#     finally:
#         zno2019_file.close()
#         con.commit()

cur.execute(r'''DROP TABLE IF EXISTS table1; SELECT regname, round(avg(ukrball100)) AS year_2020 INTO TABLE1 FROM zno WHERE year = 2020 GROUP BY regname; DROP TABLE IF EXISTS table2; SELECT regname, round(avg(ukrball100)) AS year_2019 INTO TABLE2 FROM zno WHERE Year = 2019 GROUP BY regname; SELECT TABLE1.REGNAME, TABLE1.year_2020, TABLE2.year_2019 FROM TABLE1, TABLE2 WHERE TABLE1.REGNAME = TABLE2.REGNAME ORDER BY TABLE1.year_2020 DESC;''')

rows = cur.fetchall() # Читаем содержимое запроса (прошлая команда)

with open('result.csv', 'w', newline='') as fp:
    result_file = csv.writer(fp, delimiter=';', quoting=csv.QUOTE_ALL, quotechar='"', doublequote=True, lineterminator='\n')
    result_file.writerows(rows)

print('\nРегіон;', 'середній бал за 2019 р.;', 'середній бал за 2020 р.''')
for row in rows:
	x_2019 = row[2]
	x_2020 = row[1]
	if len(str(x_2019)) == 4:
		print(row[0], ';', x_2019, ';', x_2020)
	elif len(str(x_2019)) == 5:
		print(row[0], ';', x_2019, ';', x_2020)
	else:
		print(row[0], ';', x_2019, ';', x_2020)

con.commit() # Метод commit() помогает нам применить изменения, которые мы внесли в базу данных

con.close() # Метод close() закрывает соединение с базой данных.